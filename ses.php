<?php

// edit ses

function ses($x = array()) {

    
    if (empty($x)) {
        $x[] = 130;
        $x[] = 145;
        $x[] = 150;
        $x[] = 165;
        $x[] = 170;
        $x[] = 170;
    }


    $n = count($x);
    $alpha = 2 / ($n + 1);
    $F = array();
    
    foreach ($x as $t => $value) {        
        if ($t == 0) {
            $F[$t] = $x[$t];
        }else{
            $F[$t] = $F[$t-1] + $alpha*($x[$t-1]-$F[$t-1]);
        }
        
        if ($t <= 1) {
            $mse[$t] = 0;
            $mad[$t] = 0;
        } else {
            $mse[$t]=  pow(($value - $F[$t]),2);
            $mad[$t] = abs($value - $F[$t]);
        }
        $mape[$t] = ($mad[$t] / $value) * 100;            
    }

    $data['t'] = $t;
    $data['F'] = $F;
    $data['mad'] = $mad;
    $data['mse'] = $mse;
    $data['mape'] = $mape;

    return $data;
}